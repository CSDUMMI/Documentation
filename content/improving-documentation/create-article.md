---
eleventyNavigation:
  key: CreateANewArticle
  title: How to create a new article?
  parent: ImprovingTheDocumentation
  order: 10
---

To write a new article, create a new Markdown file next to the existing files in the section that you want to put your article in. So, if for example you want to create an article called "Keeping your repo clean with .gitignore" in the "Working with Git Repositories" section, put a new file called for example `gitignore.md` into the `content/git` directory. Please adhere to the [file naming conventions](/improving-documentation/docs-contributor-faq#how-should-files-be-named%3F).

Then, add the `eleventyNavigation` header to your newly created file. That header contains information about the article's place in the structure of the site.

It can look like this:

```yaml
---
eleventyNavigation:
  key: WhatIsCodeberg
  title: What is Codeberg?
  parent: GettingStarted
  order: 10
---

## Foo
Hello foo!

## Bar
Hello bar!
```

Every section and article has a unique `key`. If `parent` is set to the `key` of another section, the current article will become a child of that other section.

The other fields are `title` and `order`, where

- `title` is merely a display name and determines the menu entry label as well as the article's heading but has no other "technical" semantics and
- `order` is a number that can be used to enforce a specific order of articles (which is good for step-by-step guides such as Getting Started for example).

So to create a new article, find the `key` of your desired parent section in its Markdown source code and use it as the `parent` for your new article.

After the header is in place, you can now start writing your article in Markdown. It is recommended to add a blank line after the header's trailing three dashes.

> Please do not repeat the article's title as a first-level heading in Markdown. It will added automatically when generating the final site.

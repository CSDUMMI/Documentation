---
eleventyNavigation:
  key: HowToMakeScreenshots
  title: How to make screenshots for Codeberg Documentation?
  parent: ImprovingTheDocumentation
  order: 15
---
Screenshots for Codeberg Documentation should follow the guidelines explained in
[Kesi Parker's excellent article](https://medium.com/technical-writing-is-easy/screenshots-in-documentation-27b45342aad8).

They should be created using the "Knut" demo account on `codeberg-test.org`. If you're a regular, trusted contributor to Documentation, you can find its credentials in a private repository in the `knut` organization on `codeberg.org`.

Otherwise, feel free to request screenshots being added when merging your PR. For that, please include `TODO SCREENSHOT [of foo]` in your article.

On the technical side, screenshots should ideally be available in both the WebP
and PNG formats (WebP and JPEG for photographs), they should be in a large enough resolution so that text is clearly
readable and their filenames should follow Codeberg Documentation's [naming conventions](/improving-documentation/docs-contributor-faq#how-should-files-be-named%3F).

Please put screenshots under `assets/images/[section]/[article]/...` where `[section]` and `[article]` are the kebab-cased names of the section or your article, respectively.

Codeberg Documentation uses the WebP format wherever possible, in order to increase page loading speed and reduce traffic due to its powerful compression.

To convert a screenshot to WebP, you can use the `cwebp` command line utility like this:

```bash
cwebp -lossless screenshot.png -o screenshot.webp
```

If you prefer a GUI, there are [a number of options](https://en.wikipedia.org/wiki/WebP#Graphics_software) available, but please be sure that your GUI doesn't add thumbnails into the WebP file, which would inflate its size, rendering its key advantage meaningless.

> At the time of writing, there is no support for webP images in Safari browsers.
> As a workaround, all images (except SVG) in Codeberg documentation are currently to be included using a `<picture>` tag, like so:
>
> ```html
> <picture>
>   <source srcset="/assets/images/getting-started/issue-tracking-basics/issues-list.webp" type="image/webp">
>   <img src="/assets/images/getting-started/issue-tracking-basics/issues-list.png" alt="Issues List">
> </picture>
> ```

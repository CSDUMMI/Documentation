---
eleventyNavigation:
  key: PreformattedText
  title: Preformatted Text
  parent: Markdown
  order: 50
---

There are two ways to use monospace preformatted text within your Markdown document:

- Using indentation
- Using one or more backticks at the beginning and the end of a preformatted section

## Using indentation

You can preformat a section of text or code by indenting the code with 4 or more spaces, or a tab:

    this
    is
    displayed
    as
    preformatted

Using indentation, it's not possible to add a rendering hint. It's also not possible to preformat text within a line using this syntax.

## Using backticks

You can preformat a section of text by starting a section of text with one or more backtick characters.

Here, we use 3 backtick characters on its own line, then our text, then another line containing 3 more backticks.

```
this
is
displayed
as
preformatted
```

You can also preformat a section of text within a line using backtick syntax.
The following text is for example `preformatted` by using the backtick syntax.

### Rendering hints

Sometime renderers use hints to syntax highlight the code in a preformatted section.

To provide a hint, simply add the language name at the end of the introductory backtick(s).

For example, using `shell` as the hint will tell the renderer that the given code should be highlighted as a shell script:

```shell
#!/bin/bash

echo "Hello world"
```

The same thing would be rendered without syntax highlighting if the hint is not given:

```
#!/bin/bash

echo "Hello world"
```

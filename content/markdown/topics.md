---
eleventyNavigation:
  key: Topics
  title: Topics
  parent: Markdown
  order: 40
---

Markdown can help you to divide a document into several parts using topics (a.k.a headings).

Topics can be specified in two ways:

- with one or more leading hash characters `#` (ATX-Style)
- by underlining a topic with dashes `-` or equal signs `=` (Setext-Style)

The Setext provides only two layers of subdivision and the ATX-Style provides up to 6.

The Codeberg documentation uses the ATX-style. In the documentation, the first topic
is omitted as it is already provided in the header section of the documentation file.
See the article on [How do I create a new article?](/improving-documentation/create-article.md)
for further details.

**Note:** This document may seem a little unstructured, as there are a bunch of topics with only a small
amount of text. Unfortunately, there is no other way to present Topics in Markdown.

### Examples of topics with hash characters

```shell
# 1st Topic
```

# 1st Topic

```shell
## 2nd Topic
```

## 2nd Topic

```shell
### 3rd Topic
```

### 3rd Topic

### Examples of topics with dashes and equal signs

```
This is a topic
===============
```

This is a topic
===============

```
This is another topic
---------------------
```

This is another topic
---------------------

